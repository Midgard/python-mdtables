#!/bin/sh

cd "`dirname "$0"`"/..
rm -rf ./build/ ./*.egg-info/ ./dist/ ./__pycache__/ ./*/__pycache__/
