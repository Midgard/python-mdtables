# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.1.0] - 2024-05-20
### Added
### Changed
- Add pipes on line start and end for greater compatibility
### Deprecated
### Removed
### Fixed
### Security

## [1.0.0] - 2024-05-20
### Added
- Release to PyPi
- Check correct usage of columns

## [0.0.1] - 2018-08-26
- Initial release
