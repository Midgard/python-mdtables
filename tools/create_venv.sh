#!/bin/sh

cd "`dirname "$0"`"/..

# Create virtualenv
python3 -m virtualenv venv/
# Install dependencies
venv/bin/pip install -e .
