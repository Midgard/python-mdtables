#!/usr/bin/env python3

from mdtables import Table, Column

t = Table(
	Column('n'),
	Column('2·n', alignment='center'),
	Column('n²', alignment='right')
)

numbers = (1, 6, 19, 59)

for n in numbers:
	t.row(n, 2*n, n*n)

print(t)
